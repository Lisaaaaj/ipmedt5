/*
 * main.c
 *
 *  Created on: 5 mei 2015
 *      Author: Lisa
 */
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <avr/wdt.h>

#define HC595_PORT PORTB
#define HC595_DDR  DDRB
#define HC595_DS_POS_SCORE PB0 // DATA
#define HC595_ST_CP_POS_SCORE PB1 // LATCH - STORE CLOCK
#define HC595_SH_CP_POS_SCORE PB2 // SCK - SHIFT CLOCK
#define HC595_DS_POS_LIGHT PB3 // DATA
#define HC595_ST_CP_POS_LIGHT PB4 // LATCH - STORE CLOCK
#define HC595_SH_CP_POS_LIGHT PB5 // SCK - SHIFT CLOCK
#define HC595_PORT_KNOP PORTD
#define HC595_DDR_KNOP DDRD
#define HC595_DS_POS_KNOP PD0 // DATA
#define HC595_ST_CP_POS_KNOP PD5 // LATCH - STORE CLOCK
#define HC595_SH_CP_POS_KNOP PD7 // SCK - SHIFT CLOCK

volatile unsigned char i; // wordt gebruikt in de ISR ivm shift register
volatile int j; // wordt gebruikt in de ISR
volatile int t=0;
volatile int p;
volatile uint8_t y,u;
volatile int score1LSeg=0;
volatile unsigned char score1RSeg=0;
volatile int score2LSeg=0;
volatile unsigned char score2RSeg=0;
volatile int goStart=0; // start het spel knop
volatile int started=0; // spel is gestart
volatile int indrukbaar=0; // alleen indrukbaar bij groene kleur weergave

//-------WATCHDOG INIT---------//
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

void wdt_init(void)
{
    MCUSR = 0;
    wdt_disable();
    return;
}
//------EINDE WATCHDOG INIT--------//

//-------SHIFT REGISTERS GEREEDMAKEN------------//
void HC595Init(){
	//Maak de DATA, SCK en LATCH output
	HC595_DDR|=((1<<HC595_SH_CP_POS_SCORE)|(1<<HC595_ST_CP_POS_SCORE)|(1<<HC595_DS_POS_SCORE));
	HC595_DDR|=((1<<HC595_SH_CP_POS_LIGHT)|(1<<HC595_ST_CP_POS_LIGHT)|(1<<HC595_DS_POS_LIGHT));
	HC595_DDR_KNOP|=((1<<HC595_SH_CP_POS_KNOP)|(1<<HC595_ST_CP_POS_KNOP)|(1<<HC595_DS_POS_KNOP));
}

//Macros om de data makkelijk te kunnen aanpassen
#define HC595DataHighScore() (HC595_PORT|=(1<<HC595_DS_POS_SCORE))
#define HC595DataLowScore() (HC595_PORT&=(~(1<<HC595_DS_POS_SCORE)))
#define HC595DataHighLight() (HC595_PORT|=(1<<HC595_DS_POS_LIGHT))
#define HC595DataLowLight() (HC595_PORT&=(~(1<<HC595_DS_POS_LIGHT)))
#define HC595DataHighKnop() (HC595_PORT_KNOP|=(1<<HC595_DS_POS_KNOP))
#define HC595DataLowKnop() (HC595_PORT_KNOP&=(~(1<<HC595_DS_POS_KNOP)))

//het verzenden van een clock pulse naar de SCK
void HC595PulseScore(){
	HC595_PORT|=(1<<HC595_SH_CP_POS_SCORE); // HIGH
	HC595_PORT&=(~(1<<HC595_SH_CP_POS_SCORE)); // LOW
}

void HC595PulseLight(){
	HC595_PORT|=(1<<HC595_SH_CP_POS_LIGHT); // HIGH
	HC595_PORT&=(~(1<<HC595_SH_CP_POS_LIGHT)); // LOW
}

void HC595PulseKnop(){
	HC595_PORT_KNOP|=(1<<HC595_SH_CP_POS_KNOP); // HIGH
	HC595_PORT_KNOP&=(~(1<<HC595_SH_CP_POS_KNOP)); // LOW
}

//Het verzenden van een pulse naar de LATCH
void HC595LatchScore(){
	HC595_PORT|=(1<<HC595_ST_CP_POS_SCORE); // HIGH
	_delay_loop_1(1);

	HC595_PORT&=(~(1<<HC595_ST_CP_POS_SCORE)); // LOW
	_delay_loop_1(1);
}

void HC595LatchLight(){
	HC595_PORT|=(1<<HC595_ST_CP_POS_LIGHT); // HIGH
	_delay_loop_1(1);

	HC595_PORT&=(~(1<<HC595_ST_CP_POS_LIGHT)); // LOW
	_delay_loop_1(1);
}

void HC595LatchKnop(){
	HC595_PORT_KNOP|=(1<<HC595_ST_CP_POS_KNOP); // HIGH
	_delay_loop_1(1);

	HC595_PORT_KNOP&=(~(1<<HC595_ST_CP_POS_KNOP)); // LOW
	_delay_loop_1(1);
}

void HC595WriteScore(uint8_t data){
	//MSB eerst
	for (int i=0;i<8;i++){
		if (data & 0b10000000){
			HC595DataHighScore();
		}
		else {
			//MSB is 0 dan output LOW
			HC595DataLowScore();
		}
		HC595PulseScore();
		data=data<<1; //verschuif bit
	}
	//breng alle data nu naar de LATCH
	HC595LatchScore();
}
void HC595WriteLight(uint8_t data){
	//MSB eerst
	for (uint8_t i=0;i<8;i++){
		if (data & 0b10000000){
			HC595DataHighLight();
		}
		else {
			//MSB is 0 dan output LOW
			HC595DataLowLight();
		}
		HC595PulseLight();
		data=data<<1; //verschuif bit
	}
}
volatile unsigned char scoreUpdateFirstSet=0;
volatile unsigned char scoreUpdateSecondSet=0;
void HC595WriteKnop(uint8_t data){
	//MSB eerst
	for (uint8_t i=0;i<8;i++){
		if (data & 0b10000000){
			HC595DataHighKnop(); // knop is ingedrukt
		}
		else {
			//MSB is 0 dan output LOW
			HC595DataLowKnop(); // knop is niet ingedrukt
		}
		HC595PulseKnop();
		data=data<<1; //verschuif bit
	}
}
//--------EINDE GEREEDMAKEN SHIFT REGISTERS----------//

//--------GEREEDMAKEN 7 SEGMENT DISPLAY---------//
uint8_t score_pattern[11]={
	// 0 is AAN
	// 1 is UIT
	// MSB is de niet gebruikte dubbele punt
	0b11000000, // 0
	0b11111001, // 1
	0b10100100, // 2
	0b10110000, // 3
	0b10011001, // 4
	0b10010010, // 5
	0b10000010, // 6
	0b11111000, // 7
	0b10000000, // 8
	0b10010000, // 9
	0b11000000 // extra bit voor het verhogen
};
//-----EINDE GEREEDMAKEN 7 SEGMENT DISPLAY------//

//-----METHODE TUSSEN WISSELEN VAN KLEURENCOMBINATIES KNOPPEN-------//
uint8_t randomStart[4]={ // wisselen tussen start kleuren
		0,
		1,
		2,
		3
};

uint8_t randomLight[5]={ // wisselen tussen spel kleurencombinaties
		0,
		1,
		2,
		3,
		4
};
//------EINDE METHODE WISSELEN VAN KLEURCOMBINATIES KNOPPEN-------//

//------AANMAKEN LEVELS DOOR KLEURENCOMBINATIES AAN TE MAKEN-------//

// 0 = aan
// 1 = uit
// MSB = Q7 & LSB = Q0

//------LEVEL 1
uint8_t lv1_light_pattern1[5]={ // knop 1 en 2 en knop 6 en 7 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11010110, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b11101101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10101101  //grbgrbgr			knop 3rg, knop 2, knop 1
};

uint8_t lv1_light_pattern2[5]={ // knop 3 en 5 en knop 8 en 10 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00101110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b10111011, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01011101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b01110110  //grbgrbgr			knop 3rg, knop 2, knop 1
};

uint8_t lv1_light_pattern3[5]={ // knop 1 en 4 en knop 6 en 9 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110101, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11011010, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b11101011, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10110101  //grbgrbgr			knop 3rg, knop 2, knop 1
};

uint8_t lv1_light_pattern4[5]={ // knop 2 en 5 en knop 7 en 10 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00101110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11010111, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01011101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10101110  //grbgrbgr			knop 3rg, knop 2, knop 1
};

uint8_t lv1_light_pattern5[5]={ // knop 1 en 3 en knop 6 en 8 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b10111010, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b11101101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b01110101  //grbgrbgr			knop 3rg, knop 2, knop 1
};

//------LEVEL 2
uint8_t lv2_light_pattern1[5]={ // knop 1 en knop 6 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11011010, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b11101101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10110101  //grbgrbgr			knop 3rg, knop 2, knop 1
};

uint8_t lv2_light_pattern2[5]={ // knop 2 en 7 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11010111, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01101101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10101110  //grbgrbgr			knop 3rg, knop 2, knop 1
};
uint8_t lv2_light_pattern3[5]={ // knop 3 en 8 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b10111011, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01101101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b01110110  //grbgrbgr			knop 3rg, knop 2, knop 1
};
uint8_t lv2_light_pattern4[5]={ // knop 4 en 9 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00110101, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11011011, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01101011, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10110110  //grbgrbgr			knop 3rg, knop 2, knop 1
};
uint8_t lv2_light_pattern5[5]={ // knop 5 en 10 groen, rest rood, neutrale knop wit
		0b11111110, //xxxxxxxb			knop 11(startknop) (x wordt niet gebruik, dus 1)
		0b00101110, //grbgrbgr			knop 11rg, knop 10, knop 9
		0b11011011, //bgrbgrbg			knop 8, knop 7, knop 6gb
		0b01011101, //rbgrbgrb        	knop 6r, knop 5, knop 4, knop 3b
		0b10110110  //grbgrbgr			knop 3rg, knop 2, knop 1
};
//------EINDE AANMAKEN LEVELS DOOR KLEURENCOMBINATIES-------//

//------GEREEDMAKEN PENDING KLEURENSHOW-------//
uint8_t beginshow3[5] = {
		0b11111110,						// alles rood
		0b01110110,
		0b11011011,
		0b01101101,
		0b10110110
};

uint8_t beginshow2[5] = {
		0b11111110,
		0b01011011,						// alles blauw
		0b01101101,
		0b10110110,
		0b11011011
};

uint8_t beginshow4[5] = {
		0b11111110,
		0b01101101,						//alles groen
		0b10110110,
		0b11011011,
		0b01101101
};

uint8_t beginshow1[5] = {
		0b11111110,
		0b01010010,
		0b01001001,						// alles paars
		0b00100100,
		0b10010010
};
//------EINDE PENDING KLEURENSHOW--------//

//------WINNAAR BEPALEN--------//
uint8_t winnaarspeler1[5] = { // SPELER 1
		0b11111110,
		0b01110110,						//speler 1 kant groen, speler 2 kant rood
		0b11011011,
		0b01011011,
		0b01101101
};

uint8_t winnaarspeler2[5] = { // SPELER 2
		0b11111110,
		0b01101101,						//speler 2 kant groen, speler 1 kant rood
		0b10110110,
		0b11101101,
		0b10110110
};
//-------EINDE WINNAAR BEPALEN------//

//------TUSSENLICHT--------//
uint8_t tussenLicht[5] = {
		0b11111110,						// alles rood
		0b00110110,
		0b11011011,
		0b01101101,
		0b10110110
};
//------EINDE TUSSENLICHT-----//

//-----METHODE VOOR WISSELEN KLEURENSHOW-----//
void startlichtshow(){

	for (y=0;y<5;y++){ // 5 shift registers

					if (randomStart[p]==0){
						HC595WriteLight(beginshow1[y]);
					}
					if (randomStart[p]==1){
						HC595WriteLight(beginshow2[y]);
					}
					if (randomStart[p]==2){
						HC595WriteLight(beginshow3[y]);
					}
					if (randomStart[p]==3){
						HC595WriteLight(beginshow4[y]);
					}
					HC595LatchLight();
	}
	p=rand()%4;
}
//-----EINDE METHODE WISSELEN KLEURENSHOW------//

//-----CHECK OF KNOP IS INGEDRUKT-------//
void checkKnop(){
	if ((PIND&(1<<PD3))>0){ // knop niet ingedrukt
		_delay_ms(10); // wacht 10 milliseconden
		if ((PIND&(1<<PD3))==0){ // check of knop is ingedrukt
			t=rand()%5;
			scoreUpdateFirstSet=1;
			score1RSeg++; // verhoog score speler 1
			for (y=0;y<5;y++){
				indrukbaar=1; // maak knop niet drukbaar ~ anders score cheat
				HC595WriteLight(tussenLicht[y]);
				HC595LatchLight();
			}
		}
	}
	if ((PIND&(1<<PD4))>0){ // knop niet ingedrukt
		_delay_ms(10); // wacht 10 milliseconden
		if ((PIND&(1<<PD4))==0){ // check of knop is ingedrukt
			t=rand()%5;
			scoreUpdateSecondSet=1;
			score2RSeg++; // verhoog score speler 2
			for (y=0;y<5;y++){
				indrukbaar=1; // maak knop niet drukbaar ~ anders score cheat
				HC595WriteLight(tussenLicht[y]);
				HC595LatchLight();
			}
		}
	}
}
//-----EINDE CHECK KNOP------//

//-----MAIN-----//
int main() {

	//common anode score borden
	DDRD |= (1<<PD1)|(1<<PD2); // OUTPUT
	PORTD |= (1<<PD1)|(1<<PD2); // HIGH - aan

	//knoppen
	DDRD &= ~(1<<PD3)|~(1<<PD4)|~(1<<PD6); // INPUT KNOP PWM
	PORTD |= (1<<PD3)|(1<<PD4)|(1<<PD6); // ENABLE INPUT

	DDRC |= (1<<PC0)|(1<<PC1); // deinieer PC0 (achtergrondmuziek) en PC1 (winnaarsgeluid) als output

	HC595Init();

	//registers TIMER2
	TCCR2B = 0b00000001;
	TIMSK2 = (1<<TOIE2);

	sei(); // start de ISR

	while(1){
		if(((PIND&(1<<PD6))>0)&&goStart==0){ // knop niet ingedrukt en spel nog niet gestart
			wdt_init(); // disable watchdog
			_delay_ms(10); // wacht 10 milliseconden
			if ((PIND&(1<<PD6))==0){
				goStart=1;
			}
		} else if (goStart==1){ // als spel gestart is
			started=1;
			if((PIND&(1<<PD6))>0){ // startknop nu reset knop
				_delay_us(250); // wacht 250 microseconden ivm anders conflict indrukbaar
				if ((PIND&(1<<PD6))==0){
					wdt_enable(WDTO_15MS); // reset ~ enable watchdog
				}
			}
			if (indrukbaar!=0){ // als speler gedrukt heeft laat kort volledig rood veld zien
				                // knoppen tijdelijk niet indrukbaar
				HC595WriteKnop(0b00000000); // knop 1 en 2 ~ 2e shift register
				HC595WriteKnop(0b00000000); // ~ 1e shift register
				HC595LatchKnop();
			}
			if(randomLight[t]==0){
				if (indrukbaar==0){ // als knoppen in te drukken zijn
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){ // level 1
						HC595WriteKnop(0b00000011); // knop 1 en 2 ~ 2e shift register
						HC595WriteKnop(0b00000011); // ~ 1e shift register
						HC595LatchKnop();
					} else { // level 2
						HC595WriteKnop(0b00000001); // knop 1
						HC595WriteKnop(0b00000001); // ~ 1e shift register
						HC595LatchKnop();
					}
				}
				checkKnop();
			} else if(randomLight[t]==1){
				if (indrukbaar==0){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteKnop(0b00010100); // knop 3 en 5 ~ 2e shift register
						HC595WriteKnop(0b00010100); // ~ 1e shift register
						HC595LatchKnop();
					} else { // level 2
						HC595WriteKnop(0b00000010); // knop 2 ~ 2e shift register
						HC595WriteKnop(0b00000010); // ~ 1e shift register
						HC595LatchKnop();
					}
				}
				checkKnop();
			} else if(randomLight[t]==2){
				if (indrukbaar==0){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteKnop(0b00001001); // knop 1 en 4 ~ 2e shift register
						HC595WriteKnop(0b00001001); // ~ 1e shift register
						HC595LatchKnop();
					} else { // level 2
						HC595WriteKnop(0b00000100); // knop 3 ~ 2e shift register
						HC595WriteKnop(0b00000100); // ~ 1e shift register
						HC595LatchKnop();
					}
				}
				checkKnop();
			} else if(randomLight[t]==3){
				if(indrukbaar==0){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteKnop(0b00010010); // knop 2 en 5 ~ 2e shift register
						HC595WriteKnop(0b00010010); // ~ 1e shift register
						HC595LatchKnop();
					} else { // level 2
						HC595WriteKnop(0b00001000); // knop 4 ~ 2e shift register
						HC595WriteKnop(0b00001000); // ~ 1e shift register
						HC595LatchKnop();
					}
				}
				checkKnop();
			} else if(randomLight[t]==4){
				if(indrukbaar==0){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteKnop(0b00000101); // knop 1 en 3 ~ 2e shift register
						HC595WriteKnop(0b00000101); // ~ 1e shift register
						HC595LatchKnop();
					} else { // level 2
						HC595WriteKnop(0b00010000); // knop 5 ~ 2e shift register
						HC595WriteKnop(0b00010000); // ~ 1e shift register
						HC595LatchKnop();
					}
				}
				checkKnop();
			}
		} else if (goStart==2){ // als spel uitgespeeld is
			if((PIND&(1<<PD6))>0){ // startknop nu reset knop
				_delay_ms(10); // wacht 10 milliseconden
				if ((PIND&(1<<PD6))==0){
					wdt_enable(WDTO_15MS); // reset ~ enable watchdog
				}
			}
		}
	}
}

ISR(TIMER2_OVF_vect){
	if(i==0){ // update scorebord
		if ((score1LSeg==4 && score1RSeg==0)||(score2LSeg==4 && score2RSeg==0)){ // the answer is 40
			PORTC &= ~(1<<PC0); // stop de achtergrondmuziek
			PORTC |= (1<<PC1); // start het winnaarsgeluid
			goStart=2;//einde spel
		}
		if(score1LSeg == 4 && score1RSeg==0)
		{
			for (y=0;y<5;y++){
				HC595WriteLight(winnaarspeler1[y]);
				HC595LatchLight();
			}
		} else if(score2LSeg == 4 && score2RSeg==0){
			for (y=0;y<5;y++){
				HC595WriteLight(winnaarspeler2[y]);
				HC595LatchLight();
			}
		} else if (score1RSeg==10){ // als i het 10e item in de array is (de extra bit)
			score1LSeg++; // verhoog linker seg met eerstvolgende item uit de array
			score1RSeg=0; // zet rechter seg weer op het eerste item van de arrray (nul)
						  // sla de extra bit dus over
		} else if (score2RSeg==10){ // als i het 10e item in de array is (de extra bit)
			score2LSeg++; // verhoog linker seg met eerstvolgende item uit de array
			score2RSeg=0; // zet rechter seg weer op het eerste item van de arrray (nul)
						  // sla de extra bit dus over
		}
		if (scoreUpdateSecondSet==1){ // score 2
			HC595WriteScore(score_pattern[score2RSeg]); // score 2 rechter digit
			HC595WriteScore(score_pattern[score2LSeg]); // score 2 linker digit
			HC595WriteScore(score_pattern[score1RSeg]); // score 1 rechter digit
			HC595WriteScore(score_pattern[score1LSeg]); // score 1 linker digit
		} else if (scoreUpdateFirstSet==1){ // score 1
			HC595WriteScore(score_pattern[score2RSeg]); // score 2
			HC595WriteScore(score_pattern[score2LSeg]); // score 2
			HC595WriteScore(score_pattern[score1RSeg]); // score 1
			HC595WriteScore(score_pattern[score1LSeg]); // score 1
		} else if (started==0) { // als er nog niet gedrukt is
			HC595WriteScore(score_pattern[0]);
		}
		scoreUpdateFirstSet=0;
		scoreUpdateSecondSet=0;
	}
	i++;
	if (goStart==0){ // spel nog niet gestart
		if (j==0){
			startlichtshow();
		}
		j++;
	}
	if (goStart==1){ // spel gestart
		PORTC |= (1<<PC0); // start achtergrondmuziek
		if (j==0){
			for (y=0;y<5;y++){ // 5 shift registers
				if (randomLight[t]==0){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){ // score 20 of lager
						HC595WriteLight(lv1_light_pattern1[y]); // level 1
					} else {
						HC595WriteLight(lv2_light_pattern1[y]); // level 2 // vanaf 20 t/m 40
					}
				} else if (randomLight[t]==1){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteLight(lv1_light_pattern2[y]); // level 1
					} else {
						HC595WriteLight(lv2_light_pattern2[y]); // level 2
					}
				} else if (randomLight[t]==2){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteLight(lv1_light_pattern3[y]); // level 1
					} else {
						HC595WriteLight(lv2_light_pattern3[y]); // level 2
					}
				} else if (randomLight[t]==3){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteLight(lv1_light_pattern4[y]); // level 1
					} else {
						HC595WriteLight(lv2_light_pattern4[y]); // level 2
					}
				} else if (randomLight[t]==4){
					if((score1LSeg<2 && score1RSeg<=10)&&(score2LSeg<2 && score2RSeg<=10)){
						HC595WriteLight(lv1_light_pattern5[y]); // level 1
					} else {
						HC595WriteLight(lv2_light_pattern5[y]); // level 2
					}
				}
				HC595LatchLight();
				indrukbaar=0; // maak knop nu weer indrukbaar nu nieuw patroon doorgestuurd is
			}
		}
		j++;
	}
}
