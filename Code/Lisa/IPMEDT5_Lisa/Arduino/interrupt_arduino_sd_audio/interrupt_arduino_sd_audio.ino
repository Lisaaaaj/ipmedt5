int speakerPin = 9; // op pin 9 wordt de speaker aangesloten.
long debouncing_time = 15; //Tijd voor debounce in miliseconden
volatile unsigned long last_micros; // Variabele voor huidige microseconden (wordt constant bijgehouden)

#include <SD.h>                      // library voor het uitlezen van de SD chip
#define SD_ChipSelectPin 10          // pin 10 wordt gebruikt voor het selecteren van de SD chip
#include <TMRpcm.h>                  // library voor de speaker
#include <SPI.h>                     // benodigde library voor de SD chip

TMRpcm tmrpcm;   // een object van de TMRpcm library voor het afspelen met de speaker

void setup()
{
  Serial.begin(9600);    // de standaard snelheid in bits per seconde waarin data over de seriele lijn gaat, ten behoeve van de console
  pinMode(2, INPUT);	   // stel de Timer0 pin in als input
  pinMode(3, INPUT);	   // stel de Timer1 pin in als input
 
  digitalWrite(2,HIGH);  // stel de pullup register in om het debounce effect te voorkomen
  digitalWrite(3,HIGH);  // stel de pullup register in om het debounce effect te voorkomen
  pinMode(speakerPin, OUTPUT); // stel de speakerpin in als output
  attachInterrupt(0, debounceInterrupt1, CHANGE); // stel de Timer0 pin in als interrupt die de debounceInterrupt1 methode aanroept voor de berekening, en roep de interrupt aan wanneer de pin van laag naar hoog of van hoog naar laag gaat
  attachInterrupt(1, debounceInterrupt2, CHANGE); // stel de Timer1 pin in als interrupt die de debounceInterrupt2 methode aanroept voor de berekening, en roep de interrupt aan wanneer de pin van laag naar hoog of van hoog naar laag gaat

 tmrpcm.speakerPin = 9; // stel de speakerpin in op 9

  if (!SD.begin(SD_ChipSelectPin)) // controleer of de micro SD kaart in de kaartlezer zit en geinitialiseerd/gelezen kan worden
  {  
    Serial.println("SD fail");  // als de micro SD kaart niet kan worden gelezen, print "SD fail"
    return;
  }
}

void loop() 
{
// blijft leeg
}  


void geluid1()
{   
  Serial.println("geluid1"); // print "geluid1" op de console ter test
   tmrpcm.play("pokemon1.wav"); // speel het bestand pokemon1.wav af
}
void geluid2()
{
  Serial.println("geluid2"); // print "geluid2" op de console ter test
   tmrpcm.play("mario1.wav"); // speel het bestand mario1.wav af
 
}

void debounceInterrupt1() // berekening dat voorkomt dat wanneer de interrupt pin (timer0) getriggerd wordt, het geluid 2 keer achter elkaar wordt afgespeeld
{
  Serial.println(micros());
  if((long)(micros() - last_micros) >= debouncing_time * 1000) 
  {
     geluid1(); // speel geluid 1 af
      last_micros = micros();    
    
  }
}

void debounceInterrupt2() // berekening dat voorkomt dat wanneer de interrupt pin (timer1) getriggerd wordt, het geluid 2 keer achter elkaar wordt afgespeeld
{
  Serial.println(micros());
  if((long)(micros() - last_micros) >= debouncing_time * 1000) 
  {
     geluid2(); // speel geluid 2 af
      last_micros = micros();    
    
  }
}
