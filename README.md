# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Bediening van de Whack-A-Mole
* V1.0

### How do I get set up? ###

* PULL eerst zodat je alle laatste wijzigingen van de online bitbucket folder op je computer download
* Ga naar je eigen WORKING COPY
* SELECTEER bij Unstaged files de bestanden met wijzigingen die je wilt committen
* Druk op COMMIT linksboven naast Checkout
* Typ wat als commit bericht - specifiek neerzetten wat (versiebeheer)

Voorbeeld bericht:

Lisa - Eerste commit - TEST - main.c - ledje gaat snel aan en uit
[je naam] - [versie commit] - [naam bestand] - [beschrijving wat je commit]
Het gaat erom dat duidelijk is wat de nieuwe commit veroorzaakt!

* Druk op PUSH naast Fetch
* Het bestand staat nu op de master - daarin kan je ook de andere wijzigingen bekijken!

### Who do I talk to? ###

* Repo owner or admin
* Inhoudelijk begeleider ipmedt5